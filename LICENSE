Author : Ewen Cameron, 5/2/2017

Unlicensed use (modifying, redistributing, reverse-engineering, etc) of any available documents or code is unauthorized in any way,
	and is subject to being politely asked to not use said documents or code in any way, at a minimum.

By installing the Sennon Arborist (sn-arborist, srb) tool through npm, you agree to the following terms and conditions of this
	version of the license, and will be referred to as the "licensee" and the author as the "licensor" of the tool going forward.

* Accessing or using any available documents or code without having installed the tool through npm does not grant the user a valid license,
	or any rights, and the user is not allowed to do anything with the available documents or code.

1. The licensor reserves the rights to modify and update the latest valid version of this license at any time and without notice,
	making that version the only valid version of the license and expiring any existing user licenses based on the previous versions of this license.
2. A licensee's current license expires automatically if a newer version of this license is published, revoking any rights the licensee has to do anything with available documents or code,
	and the new latest license must be agreed to in order to regain that version of the license's rights to do limited things with the available documents or code.
3. To have a valid license, a licensee must agree to the latest version of the license's terms and conditions, even if the latest version of the license is only made available
	by updating local documents and code.
4. This license only grants a licensee (user of the tool) the right to two limited tool uses :
	a) Install the tool through npm (only allowed way to get access to the tool, and by doing so licensee agrees to that license's terms and conditions)
	b) Use the tool as is (expected and allowed use is described by the tool's documentation, and any use considered unallowed or unexpected by the licensor expires the current license automatically,
		and revokes any rights the licensee has to do anything with the available documents or code)
5. Licensee must not modify, redistribute, reverse-engineer, etc any available documents or code related to the tool in any way.
6. Licensee must keep the latest version of the LICENSE file alongside the available documents and code at all times, and have agreed to its terms and conditions,
	or a newer latest version if the current version has expired.
7. Licensee must keep any tool copyright headers and information in available documents or code at all times.
8. Licensee will have their license expire automatically should any terms and conditions of use, as defined in any version of this license, be broken,
	also revoking any rights the licensee has to do anything with the available documents or code.
9. Any commercial use must be authorized by licensor, and any license related to unauthorized commercial use automatically expires,
	revoking any rights the licensee has to do anything with the available documents or code.
10. To the extent permitted under Law, this tool is provided under an AS-IS basis.
	a) Licensor shall never, and without any limit, be liable for any damage, cost, expense or any other payment incurred by Licensee as a result of Software’s actions, failure,
		bugs and/or any other interaction between this tool and licensee’s end-equipment, computers, other software or any 3rd party, end-equipment, computer or services.
	b) Moreover, licensor shall never be liable for any defect in source code written by licensee when relying on this tool or using this tool's source code. 
